package com.aced.caremada.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Daniil Furmanov
 * @author Jinesh Bhatt
 * @date 18/09/2018
 * Class: Remote Call
 * Description: Provides an asynchronous http requests functionality with embedded callbacks
 */

public class RemoteCall {

    public static final String JsonContentType = "application/json";

    public static final MediaType JsonMediaType = MediaType.parse(JsonContentType);

    /**
     * Verifies if Content-Type of the request is application/json
     */
    public static boolean IsJson(Request r)
    {
        return r.header("Content-Type").equals(JsonContentType);
    }

    /**
     * Verifies if Content-Type of the response is application/json
     */
    public static boolean IsJson(Response r)
    {
        return r.header("Content-Type").equals(JsonContentType);
    }

    /**
     * Interface containing methods for an asynchronous callback
     */
    public interface HttpCallback
    {
        void onSuccess(Request request, Response response);
        void onFailure(Request request, String message);
    }

    /**
     * Interface containing methods for an asynchronous callback containing JSON object
     */
    public interface HttpJsonCallback
    {
        void onSuccess(Request request, JSONObject object);
        void onFailure(Request request, String message);
    }

    private String contentType;
    private final MediaType mediaType;
    private final String URL;
    private final String postData;
    private final HttpCallback callback;

    /**
     * RemoteCall object constructor for POST method
     * @param httpCallback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     * @param data body for a POST request
     * @param type content type
     */
    public RemoteCall(final HttpCallback httpCallback, String url, String data, String type)
    {
        callback = httpCallback;
        URL = url;
        postData = data;
        contentType = type;
        mediaType = MediaType.parse(contentType);
    }

    /**
     * RemoteCall object constructor for GET method
     * @param httpCallback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     * @param type content type
     */
    public RemoteCall(final HttpCallback httpCallback, String url, String type)
    {
        callback = httpCallback;
        URL = url;
        postData = "";
        contentType = type;
        mediaType = MediaType.parse(contentType);
    }

    /**
     * RemoteCall object constructor for POST method
     * @param httpCallback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     * @param data body for a POST request
     * @param type content media type
     */
    public RemoteCall(final HttpCallback httpCallback, String url, String data, MediaType type)
    {
        callback = httpCallback;
        URL = url;
        postData = data;
        mediaType = type;
        contentType = type.toString();
    }

    /**
     * RemoteCall object constructor for GET method
     * @param httpCallback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     */
    public RemoteCall(final HttpCallback httpCallback, String url)
    {
        callback = httpCallback;
        URL = url;
        postData = "";
        contentType = JsonContentType;
        mediaType = MediaType.parse(contentType);
    }

    /**
     * Instantiates a HTTP GET request with JSON body
     * @param callback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     */
    public static void GetJson(final HttpJsonCallback callback, String url)
    {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Content-Type", RemoteCall.JsonContentType)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                try {
                    JSONObject object = new JSONObject(response.body().string());
                    callback.onSuccess(call.request(), object);
                } catch (JSONException e) {
                    callback.onFailure(request, e.getMessage());
                } catch (IOException e) {
                    callback.onFailure(request, e.getMessage());
                }
            }
        });
    }

    /**
     * Instantiates a HTTP GET request with JSON body
     * @param callback specifies an interface containing methods to callback on when a response was received
     * @param token access token
     */
    public static void GetJson(final HttpJsonCallback callback, String url, String token)
    {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("Content-Type", RemoteCall.JsonContentType)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                try {
                    JSONObject object = new JSONObject(response.body().string());
                    callback.onSuccess(call.request(), object);
                } catch (JSONException e) {
                    callback.onFailure(request, e.getMessage());
                } catch (IOException e) {
                    callback.onFailure(request, e.getMessage());
                }
            }
        });
    }

    /**
     * Instantiates a HTTP POST request with JSON body
     * @param callback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     * @param bodyContent JSON content of the request
     */
    public static void PostJson(final HttpJsonCallback callback, String url, String bodyContent)
    {
        RequestBody body = RequestBody.create(JsonMediaType, bodyContent);
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", RemoteCall.JsonContentType)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                try {
                    JSONObject object = new JSONObject(response.body().string());
                    callback.onSuccess(call.request(), object);
                } catch (JSONException e) {
                    callback.onFailure(request, e.getMessage());
                } catch (IOException e) {
                    callback.onFailure(request, e.getMessage());
                }
            }
        });
    }

    /**
     * Instantiates a HTTP POST request with JSON body
     * @param callback specifies an interface containing methods to callback on when a response was received
     * @param url remote address
     * @param bodyContent JSON content of the request
     * @param token access token
     */
    public static void PostJson(final HttpJsonCallback callback, String url, String bodyContent, String token)
    {
        RequestBody body = RequestBody.create(JsonMediaType, bodyContent);
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", RemoteCall.JsonContentType)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                try {
                    JSONObject object = new JSONObject(response.body().string());
                    callback.onSuccess(call.request(), object);
                } catch (JSONException e) {
                    callback.onFailure(request, e.getMessage());
                } catch (IOException e) {
                    callback.onFailure(request, e.getMessage());
                }
            }
        });
    }

    /**
     * Instantiates a HTTP POST request with parameters specified in the class properties
     */
    public void Post() {
        RequestBody body = RequestBody.create(mediaType, postData);
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }

    /**
     * Instantiates a HTTP POST request with parameters specified in the class properties
     * @param token access token
     */
    public void Post(String token) {
        RequestBody body = RequestBody.create(mediaType, postData);
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }

    /**
     * Instantiates a HTTP GET request with parameters specified in the class properties
     */
    public void Get() {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .get()
                .addHeader("Content-Type", contentType)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }

    /**
     * Instantiates a HTTP GET request with parameters specified in the class properties
     * @param token access token
     */
    public void Get(String token) {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .get()
                .addHeader("Content-Type", contentType)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }

    /**
     * Instantiates a HTTP HEAD request with parameters specified in the class properties
     */
    public void Head() {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .head()
                .addHeader("Content-Type", contentType)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }

    /**
     * Instantiates a HTTP HEAD request with parameters specified in the class properties
     * @param token access token
     */
    public void Head(String token) {
        final OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(URL)
                .head()
                .addHeader("Content-Type", contentType)
                .addHeader("Authorization", "Bearer " + token)
                .build();
        client.newCall(request).enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                callback.onFailure(call.request(), e.getMessage());
            }
            @Override
            public void onResponse(Call call, final Response response)
            {
                callback.onSuccess(call.request(), response);
            }
        });
    }
}