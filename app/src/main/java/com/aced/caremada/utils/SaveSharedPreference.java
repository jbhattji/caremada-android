package com.aced.caremada.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class: Save Shared Preference
 * Purpose:
 */
public class SaveSharedPreference {

    /** Values for Shared Preferences */
    private static final String LOGGED_IN_PREF = "logged_in_status";

    /** Values for Shared Preferences */
    private static final String UNIQUE_ID = "logged_in_id";

    /**
     * Default method to get shared preference
     * @param context application context
     * @return current preference
     */
    private static SharedPreferences getPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Set the Login Status
     * @param context application context
     * @param loggedIn logged in flag
     */
    public static void setLoggedIn(Context context, boolean loggedIn, String token) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean(LOGGED_IN_PREF, loggedIn);
        editor.putString(UNIQUE_ID ,token);
        editor.apply();
    }

    /**
     * Get the Login Status
     * @param context application context
     * @return boolean: login status
     */
    public static boolean getLoggedStatus(Context context) {
        return getPreferences(context).getBoolean(LOGGED_IN_PREF, false);
    }

    /**
     * Get the Session based token
     * @param context application context
     * @return string user token
     */
    public static String getUserToken(Context context) {
        String token = "";
        return getPreferences(context).getString(UNIQUE_ID, token);
    }
}
