package com.aced.caremada.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aced.caremada.R;
import com.aced.caremada.data.Login;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class:   Login Activity
 * Purpose:
 */
public class LoginActivity extends AppCompatActivity {

    /** logging tag*/
    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Log.d(TAG, "On Create: Starting Activity");
        init();
    }

    /**
     * Set content view & button listeners to launch an appropriate activity based on user interaction
     *
     */
    private void init(){
        final Button mLoginButton, mFBLoginButton;
        final EditText mEmail, mPassword;

        setContentView(R.layout.login);

        //TODO: check user inputs
        mEmail = findViewById(R.id.emailEditText);
        mPassword = findViewById(R.id.passwordEditText);

        mLoginButton = findViewById(R.id.signInButton);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mEmail.getText().toString().length() == 0) {
                    toastMessage("Email cannot be empty.");

                } else if(!mEmail.getText().toString().contains("@") || !mEmail.getText().toString().contains(".")){
                        toastMessage("Email must contain @ and .");
                } else {
                    Login newUser = new Login(mEmail.getText().toString(), mPassword.getText().toString(), LoginActivity.this);
                    newUser.userVerification();



                    Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                    startActivity(intent);

                }
            }
        });

        mFBLoginButton = findViewById(R.id.fbLoginButton);
        mFBLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Add facebook & google authentication
                //startActivity(new Intent(LoginActivity.this, FacebookActivity.class));
            }
        });
    }




    /** 8. *****************************************************************************************
     * Method Name:  toastMessage
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *      Edited:  September 27, 2018
     *   Parameter:  String message - Sentence to display in the TOAST message
     *
     *     Purpose:  Takes in a sentence and displays it to the user to help them understand
     *               why their input is not valid.
     **********************************************************************************************/
    public void toastMessage(String message){
        Toast.makeText(getApplication().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }// END of toastMessage

}
