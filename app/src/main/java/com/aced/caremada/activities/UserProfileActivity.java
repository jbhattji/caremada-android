package com.aced.caremada.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.aced.caremada.R;
import com.aced.caremada.data.UserProfile;
import com.aced.caremada.utils.RemoteCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

import okhttp3.Request;

public class UserProfileActivity extends AppCompatActivity implements RemoteCall.HttpJsonCallback {
    /** logging tag*/
    private static final String TAG = "UserProfileActivity";

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap icon = null;
            try {
                InputStream in = new java.net.URL(url).openStream();
                icon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return icon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        Log.d(TAG, "On Create: Starting Activity");
        init();
    }

    @Override
    public void onSuccess(Request request, JSONObject response) {
        try {
                    /*
                    HTTP/1.1 200 OK
                    {
                      "data": {
                        "id": 123,
                        "email": "me@email.com",
                        "profilePicture": http://www.gravatar.com/avatar/djfnsf8e8fw8efnjen9we3?size=200,
                        "registerDate": "2018-04-03 04:59:27",
                        "avgRating": 5,
                        "totalReviews": 145,
                        "fistName": "John",
                        "lastName": "Doe",
                        "dob": {
                          "year": "1990",
                          "month": "5",
                          "day": "22"
                        },
                        "phone": "1234567890",
                        "about": "This is me.",
                        "location": {
                          "addressLine1": "123 Street Name Blvd.",
                          "addressLine2": "1000",
                          "city": "Ottawa",
                          "area": "ON",
                          "postalCode": "K1P1A4",
                          "country": "CA"
                        }
                      }
                    }
                    */

            final EditText nameInput = findViewById(R.id.nameInput);
            final EditText dobInput = findViewById(R.id.dobInput);
            final EditText addressInput = findViewById(R.id.addressInput);
            final EditText cityInput = findViewById(R.id.cityInput);
            final EditText areaInput = findViewById(R.id.provinceInput);
            final EditText postalInput = findViewById(R.id.postalInput);
            final ImageView avatar = findViewById(R.id.userAvatar);

            // get the data object with all the required information
            JSONObject data = response.getJSONObject("data");
            // download & set the user image
            new DownloadImageTask(avatar).execute(data.getString("profilePicture"));
            // set the full name
            nameInput.setText(data.getString("firstName") + " " + data.getString("lastName"));
            // set the date of birth
            JSONObject dob = data.getJSONObject("dob");
            dobInput.setText(dob.getString("year") + "/" + dob.getString("month") + "/" + dob.getString("day"));
            // set the full address
            JSONObject loc = data.getJSONObject("location");
            addressInput.setText(loc.getString("addressLine1") + ";" + loc.getString("addressLine2"));
            cityInput.setText(loc.getString("city"));
            areaInput.setText(loc.getString("area"));
            postalInput.setText(loc.getString("postalCode"));

        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onFailure(Request request, String message) {
        //TODO: Add a popup message indicating no user data was recoverable
        Log.d(TAG, message);
    }

    private void init() {
        UserProfile user = new UserProfile(this, this);
        user.getUserData();
    }
}
