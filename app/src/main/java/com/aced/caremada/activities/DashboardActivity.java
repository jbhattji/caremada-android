package com.aced.caremada.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;

import com.aced.caremada.R;
import com.aced.caremada.data.Login;
import com.aced.caremada.utils.SaveSharedPreference;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class:   Dashboard Activity
 * Purpose:
 */
public class DashboardActivity extends AppCompatActivity {

    /** logging tag*/
    private static final String TAG = "DashboardActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
/*
        final Button mLogoutButton = findViewById(R.id.buttonLogout);
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent previousIntent = getIntent();
                String token = previousIntent.getStringExtra(Login.TOKEN);
                SaveSharedPreference.setLoggedIn(getApplicationContext(), false, token);

                Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

*/
        Log.d(TAG, "On Create: Starting Activity");




        CalendarView cal = new CalendarView(this);
        cal.setDate(System.currentTimeMillis(),false,true);





        // Declares and assigns the navigation bar buttons to variables for manipulation
        Button search, calendar, message, profile;
        profile = findViewById(R.id.profileButton);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, UserProfileActivity.class));
            }
        });
        search = findViewById(R.id.searchButton);
        calendar = findViewById(R.id.calendarButton);
        message = findViewById(R.id.messengerButton);
        profile = findViewById(R.id.profileButton);
    }
}
