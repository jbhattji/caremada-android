package com.aced.caremada.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import okhttp3.Request;
import okhttp3.Response;
import com.aced.caremada.R;
import com.aced.caremada.data.Registration;
import com.aced.caremada.utils.RemoteCall;
import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.aced.caremada.data.InputChecker;


/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Class Name:  RegistrationActivity.java
 *       Date:  April 20, 2018
 *   Modified:  October 5, 2018
 *    Version:  1.2
 *    @author:  Nathan M. Abbey
 *
 *    Methods:  1. onCreate(bundle savedInstanceState)
 *              2. initButton()
 *              3. specialCharCheck()
 *              4. hasUpperCase()
 *              5. hasLowerCase()
 *              6. hasLetter()
 *              7. hasNumber()
 *              8. toastMessage(String message)
 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
public class RegistrationActivity extends AppCompatActivity implements RemoteCall.HttpCallback {

    //EditText are the TEXT inputs for EMAIL, CONFIRM EMAIL, PASSWORD, and CONFIRM PASSWORD
    EditText email, email2, password, password2;

    //Button objects
    Button nextButton, backButton;

    // Server interaction
    Registration register = new Registration(this);

    // Used to manage the errors and color codes
    TextView upperChar, lowerChar, specialChar, numberChar;

    // INputUsed to verify data is correct
    InputChecker verifyInput = new InputChecker();


    /***********************************************************************************************
     * Method Name:  onSuccess
     *      Author:  Nathan M. Abbey
     *        Date:  October 9, 2018
     *
     *     Purpose:  Handles server response to email authentication
     **********************************************************************************************/
    @Override
    public void onSuccess(Request request, Response response) {
        Log.d("Response Received", response.message());
        if(response.code() == 200){

            // Highlights the border in red
           // email.setHint("Account already exists in the system.");

            // Sets the border to red color
            //email.setBackgroundResource(R.drawable.white_redborder);

            // Otherwise move on to the next screen
        } else {
            Intent intent = new Intent(RegistrationActivity.this, RegistrationActivity2.class);

            // Ensures the data is sent along to the next page.
            intent.putExtra("email", email.getText().toString());
            intent.putExtra("password", password.getText().toString());
            startActivity(intent);

        }
    }



    /***********************************************************************************************
     * Method Name:  onFailure
     *      Author:  Nathan M. Abbey
     *        Date:
     *
     *     Purpose:  Server request failed
     **********************************************************************************************/
    @Override
    public void onFailure(Request request, String message){
        Log.e("Response Failed", message);
        toastMessage("Network Connectivity Issue");
    }




    /** 1. *****************************************************************************************
     * Method Name:  onCreate
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *     Revised:  Sept 23, 2018
     *
     *     Purpose:  Acts as the constructor and sets up the elements in the Activity
     **********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        //Registers all of the input values from the LAYOUT file associated w/ this Java file
        email = findViewById(R.id.emailInput);
        email2 = findViewById(R.id.confirmEmailInput);
        password = findViewById(R.id.passwordInput);
        password2 = findViewById(R.id.confirmPasswordInput);

        //Sets up the Button and calls the method initButton() to give it functionality
        nextButton = findViewById(R.id.next_button);
        backButton = findViewById(R.id.back_button);

        // Sets up the errors messag formatting for password
        upperChar = findViewById(R.id.upperCharacter);
        lowerChar = findViewById(R.id.lowerCharacter);
        specialChar = findViewById(R.id.specialCharacter);
        numberChar = findViewById(R.id.numberCharacter);

        initButton();

    } // END of onCreate



    /** 2. *****************************************************************************************
     * Method Name:  initButton
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *     Revised:  September 22, 2018
     *               September 27, 2018 - added 'errorMessageColours()' method
     *
     *     Purpose:  Sets up the buttons and will not let NEXT BUTTON continue if the data entered
     *               is not formatted properly, and BACK BUTTON will return user to Splash Screen.
     **********************************************************************************************/
    public void initButton(){


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, SplashScreenActivity.class);
                startActivity(intent);

            }
        });

        // Sets the onClickListener - the logic for the the Button click
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String passw = password.getText().toString();

                errorMessageColours();
                // Ensures all inputs aren't empty
                if(email.getText().length() == 0 && email2.getText().length() == 0 &&
                        password.getText().length() == 0 && password2.getText().length() == 0){
                    toastMessage("The fields cannot be left empty");

                // Ensures the email length is longer than 7 characters
                }else if(email.getText().toString().length() < 7) {
                    toastMessage("Email min length is 7");

                // Ensures the email has a '@' and a '.'
                } else if(!email.getText().toString().contains("@") || !email.getText().toString().contains(".")){
                    toastMessage("Email must contain @ and .");

                    // Ensures the emails match
                } else if(!email.getText().toString().equals(email2.getText().toString())) {
                    toastMessage("Emails need to match");

                    // Ensures the password length is longer than 8 characters
                } else if(password.getText().toString().length() < 8) {
                    toastMessage("Password min length is 8");

                    // Ensures the password contains at least 1 letter
                }else if(!hasLetter()) {
                    toastMessage("Password needs to contain a letter");

                    // Ensures the password contains at least 1 upper case letter
                } else if(!hasUpperCase(password.getText().toString())){
                    toastMessage("Password needs an upper case letter");

                    // Ensures the password contains at least 1 lower case letter
                } else if(!hasLowerCase(password.getText().toString())){
                    toastMessage("Password needs a lower case letter");

                    // Ensures the password contains at least 1 number
                }else if(!hasNumber(password.getText().toString())){
                    toastMessage("Password needs to contain a number");

                    // Ensures the password has a special character
                }else if(!verifyInput.specialCharCheck(passw)) {
                    toastMessage("Password must contain a special character");

                // Ensures the passwords match
                } else if (!password.getText().toString().equals(password2.getText().toString())) {
                    toastMessage("Passwords need to match");

                } else {

                    // Validates email and error is it fails; moves on if successful
                    register.validateEmail(email.getText().toString());
                }
            }
        });
    } // END of initButton



    /** 3. *****************************************************************************************
     * Method Name:  specialCharCheck
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018
     *
     *     Purpose:  Ensures the PASSWORD has a special character within it
     **********************************************************************************************/
    public boolean specialCharCheck(String s){

        boolean correctFormat = true;
        boolean specialBoolean = true;

        Pattern nonSpecialChar = Pattern.compile("[^A-Za-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher match = nonSpecialChar.matcher(s.toString());

        specialBoolean = match.find();

        if(specialBoolean == false) {
            correctFormat = false;

        }
        return correctFormat;
    }





    /** 4. *****************************************************************************************
     * Method Name:  hasUpperCase
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018
     *
     *     Purpose:  Ensures the PASSWORD has uppercase letter(s) within it
     **********************************************************************************************/
public boolean hasUpperCase(String s){

    boolean upperCase = false;
    String passwordChecker = s.toString();

    for(int i = 0; i < passwordChecker.length(); i++) {
        if(Character.isUpperCase(passwordChecker.charAt(i))) {
            upperCase = true;
        }
    }
    return upperCase;
}



    /** 5. *****************************************************************************************
     * Method Name:  hasLowerCase
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018
     *
     *     Purpose:  Ensures the PASSWORD has lowercase letter(s) within it
     **********************************************************************************************/
    public boolean hasLowerCase(String s){

        boolean lowerCase = false;
        String passwordChecker = s.toString();

        for(int i = 0; i < passwordChecker.length(); i++) {

            if(Character.isLowerCase(passwordChecker.charAt(i))) {
                lowerCase = true;
            }
        }
        return lowerCase;
    }



    /** 6. *****************************************************************************************
     * Method Name:  hasLetter
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018
     *
     *     Purpose:  Ensures the PASSWORD has letter(s) within it
     **********************************************************************************************/
    public boolean hasLetter(){

        boolean hasLetter = false;
        String test = password.getText().toString();

        for(int i = 0; i < test.length(); i++) {

            if (Character.isLetter(test.charAt(i))) {
                hasLetter = true;
                break;
            }
        }

        return hasLetter;
    }




    /** 7. *****************************************************************************************
     * Method Name:  hasNumber
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018
     *
     *     Purpose:  Ensures the PASSWORD has number(s) within it
     **********************************************************************************************/
    public boolean hasNumber(String s){

        boolean hasNumber = false;
        String passwordTester = s.toString();

        for(int i = 0; i < passwordTester.length(); i++) {

            if(Character.isDigit(passwordTester.charAt(i))){
                hasNumber = true;
                break;
            }
        }
        return hasNumber;
    }



    /** 8. *****************************************************************************************
     * Method Name:  toastMessage
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *      Edited:  September 27, 2018
     *   Parameter:  String message - Sentence to display in the TOAST message
     *
     *     Purpose:  Takes in a sentence and displays it to the user to help them understand
     *               why their input is not valid.
     **********************************************************************************************/
    public void toastMessage(String message){
        Toast.makeText(getApplication().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }// END of toastMessage





public void errorMessageColours(){

    if(!hasUpperCase(password.getText().toString()))
        upperChar.setTextColor(Color.RED);
    else
        upperChar.setTextColor(Color.GREEN);


    if(!hasLowerCase(password.getText().toString()))
        lowerChar.setTextColor(Color.RED);
    else
        lowerChar.setTextColor(Color.GREEN);


    if(!hasNumber(password.getText().toString()))
        numberChar.setTextColor(Color.RED);
    else
        numberChar.setTextColor(Color.GREEN);


   if (!specialCharCheck(password.getText().toString()))
       specialChar.setTextColor(Color.RED);
   else
       specialChar.setTextColor(Color.GREEN);

}





} // END of class
