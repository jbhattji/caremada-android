package com.aced.caremada.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.aced.caremada.R;
import com.aced.caremada.data.Registration;
import com.aced.caremada.utils.RemoteCall;
import java.util.ArrayList;
import okhttp3.Request;
import okhttp3.Response;

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Class Name:  RegistrationActivity3.java
 *       Date:  April 12, 2018
 *    @author:  Nathan M. Abbey
 *
 *    Methods:  1. onCreate(bundle savedInstanceState)
 *              2. addProvinces()
 *              3. initButton()
 *              4. register(Registation user)
 *              5. postCodeFormatter()
 *              6. toastMessage(String message)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
public class RegistrationActivity3 extends AppCompatActivity implements RemoteCall.HttpCallback {

    // Registers the items from the View layout associated w/ this page
    Spinner provChoice;
    EditText address1, address2, city, postalCode;
    CheckBox accept;
    Button submit;
    Button backButton;
    TextView termsConditions;

    String mail, passw, fName, lName, phoneNum, yr, mnth, da;

    // Server interaction
    Registration register = new Registration(this);



    /***********************************************************************************************
     * Method Name:  onSuccess
     *      Author:  Nathan M. Abbey
     *        Date:
     *
     *     Purpose:  Handles server response
     **********************************************************************************************/
    @Override
    public void onSuccess(Request request, Response response){
        Log.d("Response Success", response.message());
        if(response.isSuccessful()){

            Intent intent = new Intent(RegistrationActivity3.this, LoginActivity.class);
            startActivity(intent);

        }else{
            //TODO:Receive proper error
            // Failed to create
            // Currently this means duplicate entry - ignore for now
        }
    }

    String mailPasser, passwordPasser, firstNamePasser, lastNamePasser, phoneNumberPasser, yearPasser, monthPasser, dayPasser;

    /***********************************************************************************************
     * Method Name:  onFailure
     *      Author:  Nathan M. Abbey
     *        Date:
     *
     *     Purpose:  Server request failed
     **********************************************************************************************/
    @Override
    public void onFailure(Request request, String message){
        Log.d("Response Failed", message);
    }



    /** 1. *****************************************************************************************
     * Method Name:  onCreate
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Acts as the constructor and sets up the elements in the Activity
     **********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration3);

        // Takes the values from the last View and assigns them to String values
        Bundle extra = getIntent().getExtras();
        final String email = extra.getString("email");
        final String password =  extra.getString("password");
        final String firstName = extra.getString("fName");
        final String lastName = extra.getString("lName");
        final String phoneNumber = extra.getString("phone");
        final String year = extra.getString("year");
        final String month = extra.getString("month");
        final String day = extra.getString("day");

         mail = email;
         passw = password;
         fName = firstName;
         lName = lastName;
         phoneNum = phoneNumber;
         yr = year;
         mnth = month;
         da = day;

        // Variables used to pass values to the JSON
         mailPasser = email;
         passwordPasser = password;
         firstNamePasser = firstName;
         lastNamePasser = lastName;
         phoneNumberPasser = phoneNumber;
         yearPasser = year;
         monthPasser = month;
         dayPasser = day;

        // Registers the province Spinner and adds the provinces
        provChoice = findViewById(R.id.provSpinner);
        addProvinces();

        termsConditions = findViewById(R.id.termsCond);


        // Registers the POSTAL CODE input and adds ghe Text Listener
        postalCode = findViewById(R.id.postalInput);
        postalCodeFormatter();

        address1 = findViewById(R.id.add1Input);
        address2 = findViewById(R.id.add2Input);
        accept = findViewById(R.id.checkBox);
        city = findViewById(R.id.cityInput);
        provChoice = findViewById(R.id.provSpinner);

        submit = findViewById(R.id.submit_button);
        initButton();

       // backButton = findViewById(R.id.back_button);
        //initBackButton();
    }



    /** 2. *****************************************************************************************
     * Method Name:  addProvinces()
     *      @author  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Adds the provinces to the spinner
     **********************************************************************************************/
    public void addProvinces(){

        ArrayList<String> list = new ArrayList<>();
        list.add("AB");
        list.add("BC");
        list.add("MAN");
        list.add("NB");
        list.add("NL");
        list.add("NS");
        list.add("NWT");
        list.add("NU");
        list.add("ON");
        list.add("PEI");
        list.add("QC");
        list.add("SSK");
        list.add("YU");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        provChoice.setAdapter(adapter);
    }



    /** 3. *****************************************************************************************
     * Method Name:  initButton()
     *      @author  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Provides functionality to the NEXT Button
     **********************************************************************************************/
    public void initButton(){

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(address1.getText().toString().trim().length() == 0){
                    toastMessage("Address 1 cannot be empty");

                } else if(city.getText().toString().trim().length() == 0) {
                    toastMessage("City cannot be left empty");

                } else if(postalCode.getText().toString().trim().length() == 0) {
                    toastMessage("Postal code cannot be empty ");

                } else if(!postalCodeFormatter()) {
                        toastMessage("Postal code not correct format");

                } else if(!accept.isChecked()){
                    toastMessage("Please agree to terms and conditions");

                }   else {

                    String city2=city.getText().toString();
                    String birthdate = monthPasser + " " + dayPasser + " " + yearPasser;
                    String province = provChoice.getSelectedItem().toString();

                    register.createAccount(mailPasser, passwordPasser, firstNamePasser, lastNamePasser, birthdate,
                            phoneNumberPasser, address1.getText().toString(), address2.getText().toString(), city2,
                            province, postalCode.getText().toString() );
                }
            }
        });
    }



    /**
     * Attempts user account creation
     *
     * @param user
     * @author Nathan Festoso
     */
    public void register(Registration user){

    }



    /** 5. *****************************************************************************************
     * Method Name:  postalCodeFormatter
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *
     *     Purpose:  Visually formats the phone number input for easier user by uer
     **********************************************************************************************/
    public boolean postalCodeFormatter(){

        String postalCodeChecker = postalCode.getText().toString().replace(" ", "");

        boolean properFormat = true;

        try {
            if (!Character.isLetter(postalCodeChecker.charAt(0)))
                properFormat = false;

            if (!Character.isDigit(postalCodeChecker.charAt(1)))
                properFormat = false;

            if (!Character.isLetter(postalCodeChecker.charAt(2)))
                properFormat = false;

            if (!Character.isDigit(postalCodeChecker.charAt(3)))
                properFormat = false;

            if (!Character.isLetter(postalCodeChecker.charAt(4)))
                properFormat = false;

            if (!Character.isDigit(postalCodeChecker.charAt(5)))
                properFormat = false;

        } catch (Exception e){
            properFormat = false;
        }
        return properFormat;
    }



    /** 5. *****************************************************************************************
     * Method Name:  toastMessage
     *      @author  Nathan M. Abbey
     *        Date:  April 12, 2018
     *       @param  message  - Sentence to display in the TOAST message
     *
     *     Purpose:  Takes in a sentence and displays it to the user to help them understand
     *               why their input is not valid.
     **********************************************************************************************/
    public void toastMessage(String message){
        Toast.makeText(getApplication().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }


    }

