package com.aced.caremada.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.aced.caremada.R;
import com.aced.caremada.utils.SaveSharedPreference;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class:   Splash Screen Activity
 * Purpose:
 */
public class SplashScreenActivity extends AppCompatActivity {

    /** logging tag*/
    private static final String TAG = "SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "On Create: Starting Activity");

        super.onCreate(savedInstanceState);

        // check if local session has a user already logged in
        if (SaveSharedPreference.getLoggedStatus(getApplicationContext())) {
            startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
        } else {
            init(SplashScreenActivity.this);
        }
    }

    /**
     * Set content view & button listeners to launch an appropriate activity based on user interaction
     * @param context application context
     */
    private void init(Context context){
        Button mLoginButton, mSignUpButton;

        setContentView(R.layout.splash_screen);

        mLoginButton = findViewById(R.id.signInButton);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            }
        });

        mSignUpButton = findViewById(R.id.signUpButton);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashScreenActivity.this, RegistrationActivity.class));
            }
        });

        // simply check for network connectivity
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        //boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
//
//        if (!isConnected) {
//            Log.d(TAG, "Connection not spotted");
//            mLoginButton.setEnabled(false);
//            mSignUpButton.setEnabled(false);
//        }
        Log.d(TAG, "Connection spotted");
    }

   @Override
    protected void onStart() {
        Log.d(TAG, "On Start");
        super.onStart();
    }

    @Override
    protected void onRestart(){
        Log.d(TAG, "On Restart");
        super.onRestart();
    }

    @Override
    protected void onResume(){
        Log.d(TAG, "On Resume");
        super.onResume();
    }

    @Override
    protected void onPause(){
        Log.d(TAG, "On Pause");
        super.onPause();
    }

    @Override
    protected void onStop(){
        Log.d(TAG, "On  Stop");
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        Log.d(TAG, "On Destroy");
        super.onDestroy();
    }
}
