package com.aced.caremada.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.aced.caremada.R;
import com.aced.caremada.data.Registration;
import com.aced.caremada.utils.RemoteCall;
import java.util.ArrayList;
import java.util.Calendar;
import okhttp3.Request;
import okhttp3.Response;

/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Class Name:  RegistrationActivity2.java
 *       Date:  April 20, 2018
 *    @author:  Nathan M. Abbey
 *
 *    Methods:  1. onCreate(bundle savedInstanceState)
 *              2. addYears()
 *              3. addMonths()
 *              4. addDays()
 *              5. nextButton(final String email, final String password)
 *              6. monthMax(String month, int day, int year)
 *              7. phoneFormatter()
 *              8. toastMessage(String message)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
public class RegistrationActivity2 extends AppCompatActivity implements RemoteCall.HttpCallback {

    // EditText are the inputs for PHONE, FIRST NAME, and LAST NAME
    EditText phone, firstName, lastName;

    //Spinners input to handle YEAR, MONTH< and DAY
    Spinner selectYear, selectMonth, selectDay;


    // NEXT and BACK Button
    Button next, backButton;


    // Server interaction
    Registration register = new Registration(this);

    Bundle extra;
    String email;
    String password;


    /***********************************************************************************************
     * Method Name:  onSuccess
     *      Author:  Nathan M. Abbey
     *        Date:
     *
     *     Purpose:  Handles server response
     **********************************************************************************************/
    @Override
    public void onSuccess(Request request, Response response) {
        Log.d("Response Received", response.message());
        if(response.code() == 200){

            // Displays a prompt to user
            phone.setHint("Phone number exists in the system.");
            // Sets the border to red color
            phone.setBackgroundResource(R.drawable.white_redborder);
        }else {

            Intent intent = new Intent(RegistrationActivity2.this, RegistrationActivity3.class);


            // Sets the data inputted to be sent to the next page
            intent.putExtra("email", email);
            intent.putExtra("password", password);
            intent.putExtra("fName", firstName.getText().toString());
            intent.putExtra("lName", lastName.getText().toString());
            intent.putExtra("phone", phone.getText().toString());
            intent.putExtra("year", selectYear.getSelectedItem().toString());
            intent.putExtra("month", selectMonth.getSelectedItem().toString());
            intent.putExtra("day", selectDay.getSelectedItem().toString());
            startActivity(intent);

        }
    }



    /***********************************************************************************************
     * Method Name:  onFailure
     *      Author:  Nathan M. Abbey
     *        Date:
     *
     *     Purpose:  Server request failed
     **********************************************************************************************/
    @Override
    public void onFailure(Request request, String message){
        Log.e("Response Failed", message);
        toastMessage("Network connectivity issue.");
    }



    /** 1. *****************************************************************************************
     * Method Name:  onCreate
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Acts as the constructor and sets up the elements in the Activity
     **********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration2);

        // Takes the values from the last View and assigns them to String values
        extra = getIntent().getExtras();
        email = (String) extra.getString("email");
        password = (String) extra.getString("password");

        // Registers the input fields for FIRST NAME and LAST NAME
        firstName = findViewById(R.id.firstNameInput);
        lastName = findViewById(R.id.lastNameInput);

        // Registers the PHONE input and sets the formatting for it
        phone = findViewById(R.id.phoneInput);
        phoneFormatter();


        // Sets up the YEAR spinner and inserts years
        selectYear = findViewById(R.id.yearSpinner);
        addYears();

        // Sets up the MONTH spinner and inserts months
        selectMonth = findViewById(R.id.monthSpinner);
        addMonths();

        // Sets up DAY spinner and inserts days
        selectDay = findViewById(R.id.daySpinner);
        addDays();


        // Sets up the NEXT button and 2 EXTRA Strings from the previous page too
        next = findViewById(R.id.next_button);
        //email and password were provided from the previous View
        nextButton(email, password);

        backButton = findViewById(R.id.back_button);
        initBackButton();

    }




    /** 2. *****************************************************************************************
     * Method Name:  addYears
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Adds the years to the YEAR Spinner
     **********************************************************************************************/
    public void addYears(){

        int tempYear = Calendar.getInstance().get(Calendar.YEAR);
        int firstYear = tempYear - 100;
        ArrayList<String> list = new ArrayList<>();

        for (int i = tempYear; i > firstYear; i--){
            list.add(String.valueOf(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        selectYear.setAdapter(adapter);
    }



    /** 3. *****************************************************************************************
     * Method Name:  addMonths
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Adds the months to the MONTH Spinner
     **********************************************************************************************/
    public void addMonths(){

        ArrayList<String> list = new ArrayList<>();
        list.add("Jan");
        list.add("Feb");
        list.add("Mar");
        list.add("Apr");
        list.add("May");
        list.add("Jun");
        list.add("Jul");
        list.add("Aug");
        list.add("Sep");
        list.add("Oct");
        list.add("Nov");
        list.add("Dec");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        selectMonth.setAdapter(adapter);
    }




    /** 4. *****************************************************************************************
     * Method Name:  addDays
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *
     *     Purpose:  Adds the days to the DAY Spinner
     **********************************************************************************************/
    public void addDays(){

        ArrayList<String> list = new ArrayList<>();

        for (int i = 1; i < 32; i++){
            list.add(String.valueOf(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        selectDay.setAdapter(adapter);
    }




    /** 5. *****************************************************************************************
     * Method Name:  nextButton
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *       @param  email - email input in last page
     *       @param  password - password input from last page
     *
     *     Purpose:  Takes in a Button and sets up the logic behind it.
     **********************************************************************************************/
    public void nextButton(final String email, final String password){

        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // Retrieves the values from the inputs in this View
                int yearChose = Integer.parseInt(selectYear.getSelectedItem().toString());
                int dayChose = Integer.parseInt(selectDay.getSelectedItem().toString());
                String monthChose = selectMonth.getSelectedItem().toString();
                String phone2 = phone.getText().toString();

                // Ensures none of the input parameters are empty
                if(firstName.getText().toString().trim().length() == 0){
                    toastMessage("First name cannot be empty");
                }
                else if(lastName.getText().toString().trim().length() == 0){
                    toastMessage("Last name cannot be empty");
                }

                // Ensures the day selected for the month exists in that month (need to add LEAP YEAR verification)
                else if(!dateExists(monthChose, dayChose, yearChose)){
                    toastMessage("That date does not exist");
                }

                // Ensures the phone number is not empty
                else if(phone.getText().toString().trim().length() == 0){
                    toastMessage("Phone number cannot be empty");

                    // If data is valid carry on to next View
                } else {

                    register.validatePhone(phone2);

                }
            }
        });
    }



    /** 6. *****************************************************************************************
     * Method Name:  dateExists
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *       @param  month - The month user chose
     *       @param  day - The day of the month user chose
     *       @param  year - The year user chose
     *
     *     Purpose:  Takes in MONTH, DAY and YEAR and checks to see if the day exists
     **********************************************************************************************/
    public boolean dateExists(String month, int day, int year) {

        boolean result = true;
        boolean isLeapYear = false;

        // Checks to see if the year is a LEAP YEAR or not
        if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
            isLeapYear = true;
        else
            isLeapYear = false;

        switch(month) {

            case "Feb":

                if(day > 28){
                    if(day == 29 && isLeapYear == true){
                        result = true;
                    } else {
                        result = false;
                    }
                }
                break;

            case "Apr":
                if(day > 30){
                    result = false;
                } else {
                    result = true;
                }
                break;

            case "Jun":
                if(day > 30){
                    result = false;
                } else {
                    result = true;
                }
                break;


            case "Sep":
                if(day > 30){
                    result = false;
                } else {
                    result = true;
                }
                break;


            case "Nov":
                if(day > 30){
                    result = false;
                } else {
                    result = true;
                }
                break;
        }
        return result;
    }



    /** 7. *****************************************************************************************
     * Method Name:  phoneFormatter
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *
     *     Purpose:  Visually formats the phone number input for easier user by uer
     **********************************************************************************************/
    public void phoneFormatter(){

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.toString().length() == 1){
                    phone.removeTextChangedListener(this);
                    String str = "(";

                    if(phone.getText().toString().equals("("))
                        str = "";

                    phone.setText(str + s.toString());
                    phone.setSelection(phone.getText().toString().length());
                    phone.addTextChangedListener(this);
                }

                if(s.toString().length() == 4){
                    phone.removeTextChangedListener(this);
                    String str = ") ";
                    phone.setText(s.toString() + str);
                    phone.setSelection(phone.getText().toString().length());
                    phone.addTextChangedListener(this);
                }

                if(s.toString().length() == 5){
                    phone.removeTextChangedListener(this);
                    String str = "";
                    phone.setText(str);
                    phone.addTextChangedListener(this);
                }

                if(s.toString().length() == 9){
                    phone.removeTextChangedListener(this);
                    String str = " - ";
                    phone.setText(s.toString() + str);
                    phone.addTextChangedListener(this);
                    phone.setSelection(phone.getText().toString().length());
                }

                if(s.toString().length() == 11){
                    phone.removeTextChangedListener(this);
                    String str = "";
                    phone.setText(str);
                    phone.addTextChangedListener(this);
                }
            }
        });
    }



    /** 8. *****************************************************************************************
     * Method Name:  toastMessage
     *      Author:  Nathan M. Abbey
     *        Date:  April 12, 2018
     *   Parameter:  String message - Sentence to display in the TOAST message
     *
     *     Purpose:  Takes in a sentence and displays it to the user to help them understand
     *               why their input is not valid.
     **********************************************************************************************/
    public void toastMessage(String message){
        Toast.makeText(getApplication().getBaseContext(), message, Toast.LENGTH_SHORT).show();
    }



    public void initBackButton(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity2.this, RegistrationActivity.class);
                startActivity(intent);

            }
        });
    }
}
