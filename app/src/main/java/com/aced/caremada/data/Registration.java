package com.aced.caremada.data;

import com.aced.caremada.utils.RemoteCall;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * This class handles registration verification and submission
 *
 * @author Nathan Festoso
 * @version 0.1
 */
public class Registration {

    // Callback activity
    private RemoteCall.HttpCallback callback;

    // API connection
    private final String URL = "https://ambitiously-aurochs-microfarad.herokuapp.com/users";
    private final String TAG = "Registration";


    /**
     * Constructor
     *
     * @author Nathan Festoso
     */
    public Registration(RemoteCall.HttpCallback callback) {
        this.callback = callback;
    }


    /**
     * Check if email already exists
     *
     * @author Nathan Festoso
     */
    public void validateEmail(String email) {
        // Set URL parameters
        String params = "/email/"+email;

        // Send HEAD request to
        RemoteCall remoteCall = new RemoteCall(callback,URL+params);
        remoteCall.Head();
    }


    /**
     * Check if phone number already exists
     *
     * @author Nathan Festoso
     */
    public void validatePhone(String number) {
        // Set URL parameters
        String params = "/phone/"+number;

        // Send HEAD request to
        RemoteCall remoteCall = new RemoteCall(callback, URL+params);
        remoteCall.Head();
    }


    /**
     * Attempt to create new user account
     *
     * @author Nathan Festoso
     */
    public void createAccount(String email, String password, String firstName, String lastName, String birthDate, String phoneNumber,
                              String address1, String address2, String city, String area, String postalCode) {
        // Create JSON request body
        JSONObject postData = new JSONObject();
        try {
            postData.put("email", email);
            postData.put("password", password);
            postData.put("firstName", firstName);
            postData.put("lastName", lastName);
            postData.put("birthDate", birthDate);
            postData.put("phoneNumber", phoneNumber);
            postData.put("addressLine1", address1);
            postData.put("addressLine2", address2);
            postData.put("city", city);
            postData.put("area", area);
            postData.put("postalCode", postalCode);
            postData.put("country", "CA");
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        // Send POST request to
        RemoteCall remoteCall = new RemoteCall(callback, URL, postData.toString(), RemoteCall.JsonContentType);
        remoteCall.Post();
    }

}

