package com.aced.caremada.data.local;

import android.provider.BaseColumns;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class: DBQueries
 * Purpose:
 */
public final class DBQueries {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private DBQueries() {}

    /** DB Name*/
    public static final String DATABASE_NAME = "CACED.db";

    /** Column data type */
    private static final String TEXT_TYPE = " TEXT";


    /** Inner class that defines the user table contents */
    public static class Users implements BaseColumns {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_NAME_FIRSTNAME = "firstname";
        public static final String COLUMN_NAME_LASTNAME = "lastname";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_PHONENUMBER = "phonenumber";
    }

    /** Inner class that defines the token table contents */
    public static class Tokens implements BaseColumns {
        public static final String TABLE_NAME = "tokens";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_TOKEN= "token";
    }


    /** Db statement for creating users table*/
    public static final String SQL_CREATE_USERS =
            "CREATE TABLE IF NOT EXISTS " + DBQueries.Users.TABLE_NAME + " (" +
                    DBQueries.Users._ID + " INTEGER PRIMARY KEY," +
                    DBQueries.Users.COLUMN_NAME_FIRSTNAME + TEXT_TYPE + "," +
                    DBQueries.Users.COLUMN_NAME_LASTNAME + TEXT_TYPE + "," +
                    DBQueries.Users.COLUMN_NAME_EMAIL + TEXT_TYPE + "," +
                    DBQueries.Users.COLUMN_NAME_PHONENUMBER + " NUMERIC" +
                    " )";

    /** Db statement for creating token table*/
    public static final String SQL_CREATE_TOKEN =
            "CREATE TABLE IF NOT EXISTS " + DBQueries.Tokens.TABLE_NAME + " (" +
                    DBQueries.Tokens._ID + " INTEGER PRIMARY KEY," +
                    DBQueries.Tokens.COLUMN_NAME_USER_ID + " INTEGER" + "," +
                    DBQueries.Tokens.COLUMN_NAME_TOKEN + TEXT_TYPE +
                    " )";

    /** Db statement for deleting tables*/
    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DBQueries.Users.TABLE_NAME + ","
                                    + DBQueries.Tokens.TABLE_NAME;
}
