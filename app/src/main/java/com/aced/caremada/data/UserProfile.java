package com.aced.caremada.data;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.aced.caremada.activities.LoginActivity;
import com.aced.caremada.activities.PhoneVerificationActivity;
import com.aced.caremada.activities.UserProfileActivity;
import com.aced.caremada.utils.RemoteCall;
import com.aced.caremada.utils.SaveSharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.Request;
import okhttp3.Response;


/**
 * @author Jinesh Bhatt
 * @date 17/10/2018
 * Class: User profile
 *
 */
public class UserProfile {

    /** logging tag*/
    private static final String TAG = "UserProfileActivity";

    private WeakReference<Context> mContextReference;

    private String token;

    RemoteCall.HttpJsonCallback callback;

    private final static String URL = "https://ambitiously-aurochs-microfarad.herokuapp.com/users/me";

    JSONObject userData = new JSONObject();

    public UserProfile(RemoteCall.HttpJsonCallback cb, Context context) {
        mContextReference = new WeakReference<>(context);
        token = SaveSharedPreference.getUserToken(context);
        callback = cb;
        // for testing here is a temp token
        //token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzEzLCJpYXQiOjE1Mzk4MTQ0NDB9.wic_sQ9fYMZY36dyYyP1tCx1oxYSRU9scn_wx9DP-c4";
    }

    public void getUserData() {
        RemoteCall.GetJson(callback, UserProfile.URL, token);
    }

}
