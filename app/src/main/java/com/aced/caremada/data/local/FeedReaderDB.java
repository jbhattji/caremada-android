package com.aced.caremada.data.local;

import android.content.Context;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class: FeedReaderDB
 * Purpose:
 */
public class FeedReaderDB extends SQLiteOpenHelper {
    /** DB instance variable */
    private static FeedReaderDB instance;

    /** DB version*/
    private static final int DATABASE_VERSION = 1;

    /**
     * Default context
     * @param context application context
     */
    public FeedReaderDB(Context context) {
        super(context, DBQueries.DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Create the tables for local db
     * @param db extension to SQLite db
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBQueries.SQL_CREATE_USERS);
        db.execSQL(DBQueries.SQL_CREATE_TOKEN);
    }

    /**
     * Re-Create the tables for local db
     * @param db extension to SQLite db
     * @param newVersion verison id
     * @param oldVersion version id
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(DBQueries.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * Create a local instance and return it
     * @param context application context
     * @return a valid context based instance
     */
    public static synchronized FeedReaderDB getInstance(Context context) {
        if (instance == null) {
            instance = new FeedReaderDB(context);
        }
        return instance;
    }
}
