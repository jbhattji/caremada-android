package com.aced.caremada.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputChecker {


    public InputChecker(){
    }





    /** 1. *****************************************************************************************
     * Method Name:  specialCharCheck
     *   Parameter:  String s - Accepts in a String to determine it's contents
     *      Author:  Nathan M. Abbey
     *        Date:  April 16, 2018
     *      Edited:  September 27, 2018     - Modified to accept String input
     *               October 10, 2018       - Migrated to Java class so all activities have access
     *
     *     Purpose:  Ensures the String parameter has a special character within it
     **********************************************************************************************/
    public boolean specialCharCheck(String s){

        boolean correctFormat = true;
        boolean specialBoolean = true;

        Pattern nonSpecialChar = Pattern.compile("[^A-Za-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher match = nonSpecialChar.matcher(s.toString());

        specialBoolean = match.find();

        if(specialBoolean == false) {
            correctFormat = true;
        }
        return correctFormat;
    }
}
