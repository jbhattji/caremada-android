package com.aced.caremada.data;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.aced.caremada.activities.LoginActivity;
import com.aced.caremada.activities.PhoneVerificationActivity;
import com.aced.caremada.utils.RemoteCall;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.ref.WeakReference;
import okhttp3.Request;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class: Login
 * Purpose:
 */
public class Login implements RemoteCall.HttpJsonCallback {

    private final String email;
    private final String password;
    private final static String URL = "https://ambitiously-aurochs-microfarad.herokuapp.com/auth";
    private WeakReference<Context> mContextReference;

    /**
     * used for setting phone number tag for new intent
     */
    public static final String PHONE_NUMBER = "com.aced.caremada.activities.PHONENUMBER";

    /**
     * used for setting token tag for new intent
     */
    public static final String TOKEN = "com.aced.caremada.activities.CODE";


    /**
     * logging tag
     */
    private static final String TAG = "Login";

    public Login(String email, String password, Context context) {
        this.email = email;
        this.password = password;
        mContextReference = new WeakReference<>(context);
    }

    @Override
    public void onSuccess(Request request, JSONObject response) {

        String phoneNumber = "", token = "";

        Context context = mContextReference.get();

        Log.d(Login.TAG, response.toString());
        if (context != null) {
            if (response.toString().contains("data")) {
                try {
                    Log.d(Login.TAG, response.getJSONObject("data").getString("phoneNumber"));
                    phoneNumber = response.getJSONObject("data").getString("phoneNumber");
                    token = response.getJSONObject("data").getString("token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d(Login.TAG, phoneNumber);
                Intent intent = new Intent(context, PhoneVerificationActivity.class);
                intent.putExtra(PHONE_NUMBER, "+1" + phoneNumber);
                intent.putExtra(TOKEN, token);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                Log.d(Login.TAG, "Login was successful");
            } else {
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
                Log.d(Login.TAG, response.toString());
            }

        }
    }

    @Override
    public void onFailure(Request request, String message) {
        Intent intent = new Intent(mContextReference.get(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContextReference.get().startActivity(intent);
        Log.d(Login.TAG, message);
    }

    public void userVerification() {
        RemoteCall.PostJson(this, URL, getPostData());
    }

    public boolean fbVerification() {
        //TODO: needs to be implemented
        return true;
    }

    public boolean googleVerification() {
        //TODO: needs to be implemented
        return true;
    }

    private String getPostData() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("email", email);
            postData.put("password", password);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        return postData.toString();
    }


    /*public String getPhoneNumber() {
        return "+1" + phoneNumber;
    }
    */

    /*
    public String getToken() {
        return token;
    }
*/

}
