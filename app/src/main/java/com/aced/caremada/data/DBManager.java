package com.aced.caremada.data;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import com.aced.caremada.data.local.DBQueries;
import com.aced.caremada.data.local.FeedReaderDB;
import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

import com.aced.caremada.data.local.DBQueries;
import com.aced.caremada.data.local.FeedReaderDB;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteDatabase;

/**
 * @author Jinesh Bhatt
 * @date 17/04/2018
 * Class:   DBManager
 * Purpose:
 */
public class DBManager {

    /** application based context */
    private Context mContext;

    /** logging tag */
    private static final String TAG = "DBManager";
    private static String PASSWORD ="";

    /** temp password for db */
    private String UNIQUE_ID; //= ")#!@0231@SMD*#";

    /**
     *
     * @param context application context
     * @param key  unique key for db
     */
    public DBManager(Context context, String key){
        mContext = context;
        UNIQUE_ID = key;
        SQLiteDatabase.loadLibs(mContext);
    }

     /**
     * Inserts dummy information to db
     */
    public void insertUserToLocalDB(String firstName, String lastName, String email, String phoneNumber, String token){
        Log.d(TAG, "Password is " + UNIQUE_ID);
        long userID;

        SQLiteDatabase db = FeedReaderDB.getInstance(mContext).getWritableDatabase(UNIQUE_ID);

        ContentValues values = new ContentValues();
        values.put(DBQueries.Users.COLUMN_NAME_FIRSTNAME, firstName);
        values.put(DBQueries.Users.COLUMN_NAME_LASTNAME, lastName);
        values.put(DBQueries.Users.COLUMN_NAME_EMAIL, email);
        values.put(DBQueries.Users.COLUMN_NAME_PHONENUMBER, phoneNumber);

        userID = db.insert(DBQueries.Users.TABLE_NAME, null, values);
        values.clear();

        values.put(DBQueries.Tokens.COLUMN_NAME_USER_ID, userID);
        values.put(DBQueries.Tokens.COLUMN_NAME_TOKEN, token);

        db.insert(DBQueries.Tokens.TABLE_NAME, null, values);
        values.clear();

        Cursor cursor = db.rawQuery("SELECT * FROM '" + DBQueries.Users.TABLE_NAME + "';", null);
        Log.d(TAG, "Rows count: " + cursor.getCount());
        cursor.close();
        db.close();

        // this will throw net.sqlcipher.database.SQLiteException: file is encrypted or is not a database: create locale table failed
        //db = FeedReaderDbHelper.getInstance(this).getWritableDatabase("");
    }
}
