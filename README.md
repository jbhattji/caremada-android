# Caremada
Caremada is a place for caregiving professionals to build profiles, find work and get paid wherever they are. It allows parents, families and members to book and pay for service providers who are fully bonded and have gone through a police records check, whether at home or when traveling anywhere in the world.
